-- bell_positions are saved through server restart
-- bells ring every hour
-- they ring as many times as a bell ought to

local bell = {};
bell.positions = {};
bell.num_bells = 0;

bell.add_bell = function(pos)
	local hash = minetest.hash_node_position(pos);
	if( bell.positions[hash] ) then
		--minetest.chat_send_all("Bell exists at ".. minetest.pos_to_string(pos).."#bells: "..bell.num_bells);
	else
		bell.positions[hash] = true;
		bell.num_bells = bell.num_bells + 1;
		--minetest.chat_send_all("Bell added at ".. minetest.pos_to_string(pos).."#bells: "..bell.num_bells);
	end
end

bell.remove_bell = function(pos)
	local hash = minetest.hash_node_position(pos);
	bell.positions[hash] = nil; -- delete the bell
	bell.num_bells = bell.num_bells - 1;
	--minetest.chat_send_all("Bell removed at ".. minetest.pos_to_string(pos).."#bells: "..bell.num_bells);
end

-- Restore bell position on load
minetest.register_lbm({
	label = "index bells",
	name = "bell:find_bell",
	nodenames = {"bell:bell"},
	run_at_every_load = true,
	action = function(pos, node)
		bell.add_bell(pos);
	end,
})

--[[
	* `minetest.sound_play(spec, parameters, [ephemeral])`: returns a handle
    * `spec` is a `SimpleSoundSpec`
    * `parameters` is a sound parameter table
    * `ephemeral` is a boolean (default: false)
      Ephemeral sounds will not return a handle and can't be stopped or faded.
      It is recommend to use this for short sounds that happen in response to
      player actions (e.g. door closing).
]]
-- actually ring the bell
bell.ring_bells_once = function()

	for k,v in pairs( bell.positions ) do
	-- print("Ringing bell at "..tostring( minetest.pos_to_string( v )));
		local pos = minetest.get_position_from_hash(k)
		minetest.sound_play( "bell_bell",
				{ pos = pos, gain = 1.5, max_hear_distance = 64}, true);
	end
end

bell.ring_all_bells = function(nTimes)
	-- ring the bell for each hour once
	minetest.log("action", "[bell] Bells ringing ".. nTimes .." times.")
	for i=0, nTimes do
		minetest.after( i*5,  bell.ring_bells_once );
	end
end

bell.init = function(firstInit)
	-- figure out if this is the right time to ring
	local temp = os.date("*t");
	local sekunde = temp.sec;
	local minute  = temp.min;
	local stunde  = temp.hour %12;  -- in 12h-format (a bell that rings 24x at once would not survive long...)
	local delay   = 3600 - sekunde - (minute*60); -- calculate next full hour

	--minetest.chat_send_all("[bells]It is now H:".. stunde .." M:".. minute .." S:".. sekunde );
	--minetest.chat_send_all(dump(temp) );
	--print("[bells]It is now H:"..tostring( stunde ).." M:"..tostring(minute).." S:"..tostring( sekunde ));

	--local datum = os.date( "Stunde:%l Minute:%M Sekunde:%S");
	--print('[bells] ringing bells at '..tostring( datum ))

	-- make sure the bell rings the next hour
	minetest.after( delay, bell.init );
	minetest.log("action", ("[bell] Bells will ring in %dm %ds (%d seconds)."):format(math.floor(delay/60), delay%60, delay))

	-- on 0h and 24h ring 12 times
	if( stunde == 0) then
		stunde = 12 
	end

	if( minute == 0 and sekunde < 10 ) then
		-- Ring all bells
		bell.ring_all_bells(stunde);
	else
		if(not firstInit) then
			-- Missed the Time! can happen if singleplayer is paused.
			minetest.log("warning", ("[bell] Missed time to ring about %dm %ds!"):format(minute, sekunde))
		end
	end

end

-- first call (after the server has been started)
minetest.after(0, bell.init, true );



-- Node definition


bell.after_place = function(pos, placer)
	-- remember that there is a bell at that position
	bell.add_bell(pos);
end

bell.after_destruct = function(pos, oldnode)
	bell.remove_bell(pos);
end

bell.on_punch = function (pos, node, puncher)
	minetest.sound_play( "bell_bell", { pos = pos, gain = 1.5, max_hear_distance = 30,},true);
	--minetest.chat_send_all(puncher:get_player_name().." has rung the bell!")
end

minetest.register_node("bell:bell", {
	description = "bell",
	tiles = {"bell_bell.png"},
	paramtype = "light",
	inventory_image = 'bell_bell.png',
	wield_image = 'bell_bell.png',
	stack_max = 1,
	drawtype = "plantlike",
	--on_punch = bell.on_punch,

	after_place_node = bell.after_place,

	after_destruct = bell.after_destruct,
	sounds = {
		dig = {name="bell_bell", gain = 1.0, pitch = 2.5},
		place = {name="bell_bell", gain = 1.5, pitch = 0.8},
	},

	groups = {cracky=2},
})
